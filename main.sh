#!/bin/bash

apt-get update
apt-get install curl -y
apt-get install ffmpeg -y
apt-get update

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
apt-get install unzip -y
unzip awscliv2.zip
./aws/install

aws --version

echo "Video name id"
echo $VIDEO_NAME

aws s3 cp s3://$TEMP_BUCKET_NAME/$USER_ID/$VIDEO_ID/$VIDEO_NAME .

mvn clean
mvn package

mkdir video-segments

# Compile the Java file using the generated JAR file from Maven
javac -cp ".:/home/app/target/streamsphere-1.0.0.jar" /home/app/src/main/java/com/streamsphere/App.java

# Run your Java program
java -cp ".:/home/app/target/streamsphere-1.0.0.jar" com.streamsphere.App

