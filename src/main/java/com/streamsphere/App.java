package com.streamsphere;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import com.streamsphere.VideoDelete.S3Delete;
import com.streamsphere.VideoUpload.S3Upload;

public class App {
    public static void main(String[] args) {
        try {
            S3Upload s3Upload = new S3Upload();

            String userId = Constants.USER_ID;
            String videoId = Constants.VIDEO_ID;

            String bucketKeyPrefix = userId + "/" + videoId + "/";            

            File videoDirectory = new File("."); // Parent directory

            File transcodeVideoFolder = new File(".", "video-segments");

            if (!transcodeVideoFolder.exists()) {
                transcodeVideoFolder.mkdir();
            }

            runCommand(transcodeVideoFolder, "mkdir output_360 output_480 output_720 output_1080");

            runCommand(videoDirectory, "sh transcode-script.sh");

            
            s3Upload.uploadDirectory(transcodeVideoFolder, bucketKeyPrefix);

            S3Delete videoDelete = new S3Delete();
            videoDelete.videoDelete(userId, videoId);     

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void runCommand(File directory, String command) throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.directory(directory);
        processBuilder.command("bash", "-c", command);

        Process process = processBuilder.start();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            while ((line = errorReader.readLine()) != null) {
                System.err.println(line);
            }
        }

        int exitCode = process.waitFor();
        if (exitCode != 0) {
            throw new RuntimeException("Command failed with exit code " + exitCode);
        }
    }
}
