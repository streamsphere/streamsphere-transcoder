package com.streamsphere;

public class Constants {
    public final static String UPLOAD_SERVICE_URL = System.getenv("UPLOAD_SERVICE_URL");
    public final static String USER_ID = System.getenv("USER_ID");
    public final static String VIDEO_ID = System.getenv("VIDEO_ID");
}
