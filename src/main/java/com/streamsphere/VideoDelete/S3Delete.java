package com.streamsphere.VideoDelete;

import com.streamsphere.VideoUpload.S3Data;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;

public class S3Delete {
    private final S3Client client;

    public S3Delete() {
        this.client = S3Client.builder()
                .region(Region.AP_SOUTH_1)
                .credentialsProvider(() -> AwsBasicCredentials.create(System.getenv("AWS_ACCESS_KEY_ID"),
                        System.getenv("AWS_SECRET_ACCESS_KEY")))
                .build();
    }

    public void videoDelete(String userId, String videoId) {
        DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder()
                .bucket(S3Data.tempBucketName)
                .key(userId + "/" + videoId + "/" + System.getenv("VIDEO_NAME"))
                .build();

        client.deleteObject(deleteObjectRequest);
    }
}
