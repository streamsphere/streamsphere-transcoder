#!/bin/bash

start_time=$(date +%s)

echo "Transcoding has been started"

echo "360p begins"
# Convert video to 360p
ffmpeg -i $VIDEO_NAME -vf "scale=w=-2:h=360:force_original_aspect_ratio=decrease" -codec:v libx264 -codec:a aac -hls_time 10 -hls_playlist_type vod -hls_segment_filename video-segments/output_360/segment%03d.ts -start_number 0 video-segments/output_360/video360p.m3u8

echo "480p begins"
# # Convert video to 480p
ffmpeg -i $VIDEO_NAME -vf "scale=w=640:h=480:force_original_aspect_ratio=decrease" -codec:v libx264 -codec:a aac -hls_time 10 -hls_playlist_type vod -hls_segment_filename video-segments/output_480/segment%03d.ts -start_number 0 video-segments/output_480/video480p.m3u8

echo "720p begins"
# Convert video to 720p
ffmpeg -i $VIDEO_NAME -vf "scale=w=-2:h=720:force_original_aspect_ratio=decrease" -codec:v libx264 -codec:a aac -hls_time 10 -hls_playlist_type vod -hls_segment_filename video-segments/output_720/segment%03d.ts -start_number 0 video-segments/output_720/video720p.m3u8

echo "1080p begins"
# Convert video to 1080p
ffmpeg -i $VIDEO_NAME -vf "scale=w=-2:h=1080:force_original_aspect_ratio=decrease" -codec:v libx264 -codec:a aac -hls_time 10 -hls_playlist_type vod -hls_segment_filename video-segments/output_1080/segment%03d.ts -start_number 0 video-segments/output_1080/video1080p.m3u8

echo "Transcoding has been completed"

end_time=$(date +%s)
elapsed_time=$(($end_time - $start_time))

echo "Total time taken: $(($elapsed_time / 60)) minutes and $(($elapsed_time % 60)) seconds"